#!/usr/bin/python

import numpy as np
import npz2xml
import getopt
import sys
import os

def validate_xml(xml_file):
    result = npz2xml.validate(xml_file)
    if not result == 0:
        print result
    else:
        print "File %s is valid" % os.path.basename(xml_file)

def convert_numpy_array(source, dest, prettyprint):   

    loaded_data = np.load(source)
    arrays_dict = {}

    for fname in loaded_data.files:
        arrays_dict[fname] = loaded_data[fname]

    # else serialize npz file
    # if no dest param is set
    # than ouput to sdtout
    xnpz_string = npz2xml.serialize(arrays_dict, prettyprint)
        
    # if need save to file
    if len(dest) > 0:
        # possible trouble here - if user has
        # no rights to write at specified path
        xml_file = open(dest, 'w')
        
        xml_file.write(xnpz_string)
        
            # finally close the file
        xml_file.close()
    else:
        print xnpz_string

def deserialize(source):
    data = npz2xml.deserialize(source)
    # TODO change this in future
    print data
            
def print_help():
    print """
Usage:  npz_converter.py [OPTION] [-s|--source] sourcefile 
                         [-d|--dest] destination 

OPTIONs:
           -h, --help             print this help and exits
           -s, --source           path to source file (xml or npz)
           -d, --dest             path to destination file (ignored when validating)
                                       if no destination file is set when
                                       converting npz to xml result is sent to stdout
           -v                     checks xml file for validity
           -p                     generates xml string with tabulation
           -x, --extract          extract numpy array from xml file
                                       (if validate option is also set, 
                                        than validation runs before extraction)
"""

def main():
    try:
        opts, args = getopt.getopt(sys.argv[1:], "s:d:hvpx", \
                                       ["source=", "dest=", "extract", "help", "validate", "prettyprint"])
    except getopt.GetoptError, err:
        # print help information and exit:
        print str(err) # will print something like "option -a not recognized"
        print_help()
        sys.exit(2)

    validate = False
    prettyprint = False
    source = ""
    dest = ""
    extract = False

    for name, val in opts:
        if name in ("-v", "--validate"):
            validate = True
        elif name in ("-h", "--help"):
            print_help()
            sys.exit(0)
        elif name in ("-s", "--source"):
            source = val
        elif name in ("-d", "--dest"):
            dest = val
        elif name in ("-p", "--prettyprint"):
            prettyprint = True
        elif name in ("-x", "--extract"):
            extract = True
        else:
            print_help()
            # unknown option
            sys.exit(1)

    # check existance
    if not os.path.exists(source):
        raise IOError, "Source is not found"

    # check extraction before validation
    if extract:
        # if validate, than
        # - source param is path to xml
        # - dest param is ignored
        if validate: validate_xml(source)

        deserialize(source)
    elif validate:
        validate_xml(source)        
    else:
        convert_numpy_array(source, dest, prettyprint)
        
        
if __name__ == "__main__":
    main()
