import os
import types
import numpy as np
import numpy_helper as nh
import xml.etree.ElementTree as etree

def validate(xml_npz_filepath):
    """ Validates xml npz file at specified path. If xml is
        not valid, than return error information, otherwise 0 """
    
    if not os.path.exists(xml_npz_filepath):
        raise IOError, "Xml file not found"
    
    xml_tree = etree.parse(xml_npz_filepath)
    root = xml_tree.getroot()

    for child in list(root):
        arr_result = validate_ndarray(child)
        if not arr_result == 0:
            return arr_result
    
    return 0

def validate_ndarray(node):

    if not node.attrib.has_key('shape'):
        return "ndarray node has no required [shape] attribute (<%s> node)" % str(node)
    shape = nh.parse_tuple(node.attrib['shape'])

    if not node.attrib.has_key('dtype'):
        return "ndarray node has no required [dtype] attribute (<%s> node)" % str(node)
    array_type = nh.dtype_from_string( node.attrib['dtype'] )

    indices_hash = 0

    # TODO somehow get rid of
    # ---- hardcoded xml nodes tag names
    
    # iterate through all direct childs
    # of current node
    for child in list(node):

        parts = child.tag.split('.')
        if not len(parts) == 2:
            return """ Node tag must have format [<numpy.{type}>],
where {type} is correct numpy type (<%s> node)""" % child.tag

        if not child.attrib.has_key('coords'):
            return "Every ndarray item is required to have <coords> attribute (<%s> node)" % \
                child.tag

        coords = nh.parse_tuple(child.attrib['coords'])
        # save current index
        indices_hash ^= nh.index_to_int(coords, shape)
        
        if child.tag.endswith("void"):
            subvoid_result = validate_numpy_void(child)
            # return if subarray has some errors
            if not subvoid_result == 0:
                return subvoid_result
        else:
            # TODO also get rid sometime somehow
            # ---- from hardcoded type values in tags
            
            # try to create plain type (e.g. int32)
            # from string in node tag
            plain_type = nh.plain_dtype_from_string(parts[1])
            if plain_type is None:
                return "Node tag has incorrect type value (tag <%s> with value [%s])" %\
                    (child.tag, child.text)

            # if base type and item type are not strings
            # (due to some problems with conversion of '|S0' to '|S36')
            if not (array_type.name.startswith('string') and \
                    parts[1].startswith('string')):
                # if plain array type and element
                # type don't match, it's another error
                if plain_type != array_type:
                    return "Element types mismatch for node <%s> (array type - %s, node type - %s)" % \
                        (child.tag, array_type.descr, plain_type.descr)

                # check plain type value
                # check if value type corresponds array type
                # TODO write better checking
                if not nh.is_correct_plain_type(child.text, plain_type):
                    return "Node element type (%s) does not contain a value of that type ('%s')" % \
                        (plain_type.descr, child.text)

    # check if all indices are present
    if not indices_hash == nh.my_xor(nh.product(shape)):
        return "Error! Not all elements correspond to indices for node <%s> with dtype %s" % \
            (node.tag, array_type.descr)
            
    return 0

def validate_numpy_void(node):
    if not node.attrib.has_key('dtype'):
        return "ndarray node has no required [dtype] attribute (node <%s>)" % node.tag
    void_type = nh.dtype_from_string( node.attrib['dtype'] )

    # check all names of void subtype later
    all_names = {}
    
    # iterate through all direct childs
    # of current node
    for child in list(node):
        
        parts = child.tag.split('.')
        if not len(parts) == 2:
            return """ Node tag must have format [<numpy.{type}>],
where {type} is correct numpy type (at <%s>)""" % child.tag

        if not child.attrib.has_key('name'):
            return "Every <numpy.void> item is required to have [name] attribute (<%s>)" % \
                child.tag
        else:
            # just add this name to list of used names
            all_names[ child.attrib['name'] ] = 1

        child_name = child.attrib['name']

        if not void_type.fields.has_key(child_name):
            return "Parent dtype for node <%s> with dtype fields %s has no such field name ('%s')" % \
                (node.tag, str(void_type.fields.keys()), child_name)

        item_type = void_type[child_name]

        if child.tag.endswith("ndarray"):
            subarr_result = validate_ndarray(child)
            # return if subarray has some errors
            if not subarr_result == 0:
                return subarr_result
        elif child.tag.endswith("void"):
            subvoid_result = validate_numpy_void(child)
            # return if subarray has some errors
            if not subvoid_result == 0:
                return subvoid_result
        else:
            # TODO also get rid sometime somehow
            # ---- from hardcoded type values in tags

            # try to create plain type (e.g. int32)
            # from string in node tag
            plain_type = nh.plain_dtype_from_string(parts[1])
            if plain_type is None:
                return "Child node tag has incorrect type value (tag <%s>)" % child.tag

            # if base type and item type are not strings
            # (due to some problems with conversion of '|S0' to '|S36')
            if not (item_type.name.startswith('string') and \
                    parts[1].startswith('string')):
                # if plain array type and element
                # type don't match, it's another error
                if plain_type != item_type:
                    return "Element types mismatch (real type - %s, node type - %s)" % \
                        (array_type.descr, plain_type.descr)

                # check plain type value
                # check if value type corresponds array type
                # TODO write better checking
                if not nh.is_correct_plain_type(child.text, plain_type):
                    return "Node element type (%s) does not contain a value of that type ('%s')" % \
                        (plain_type.descr, child.text)
    
    if not void_type.fields is None:
        # if count of names, mensioned in xml
        # is not equal to names in dtype description
        if not len(all_names) == len(void_type.fields):
            return "Some named attributes are missing in node <%s> with dtype %s" % \
                (node.tag, void_type.descr)

        for key in void_type.fields.keys():
            if not all_names.has_key(key):
                return "There no element with name [%s] in node <%s> with dtype %s" % (key, node.tag, void_type.descr)

    return 0
