import os
import re
import types
import numpy as np
from xml.dom import minidom

# main method in this file
def serialize(npz_dict, prety_print=False):
    """ Converts input npz file to xml format and returns it as a string
               npz_dict : dictionary with {name => ndarray} pairs
           pretty_print : if True, prints to file with
                          tabulation. Default is False."""

    # create document objects
    xml_doc, parent_node = create_xml_doc()

    for array_name, arr in npz_dict.items():
        # serialize each array to parent node
        serialize_array(xml_doc, parent_node, \
                        arr, arr_name=array_name)

    if not prety_print:
        # return without tabulation and whitespaces
        return xml_doc.toxml()
    else:
        # default indent is 4 spaces
        uglyXml = xml_doc.toprettyxml(indent='    ')
        # remove annoying new lines, which prettyxml inserts
        text_re = re.compile('>\n\s+([^<>\s].*?)\n\s+</', re.DOTALL)    
        prettyXml = text_re.sub('>\g<1></', uglyXml)
        return prettyXml

    
def get_class_name(obj):
    return "numpy." + obj.__class__.__name__ # obj.dtype.name

def create_xml_doc():
    doc = minidom.Document()
    parent = doc.createElement("numpy.npz")
    doc.appendChild(parent)
    return (doc, parent)

# serializes one numpy.ndarray to xml minidom structure
def serialize_array(xml_doc, parent_node, arr, arr_name=None):
    arr_node = xml_doc.createElement("numpy.ndarray")
    # set attributes
    if not arr_name == None:
        arr_node.setAttribute('name', arr_name)
    arr_node.setAttribute('dtype', str(arr.dtype))
    arr_node.setAttribute('shape', str(arr.shape))

    flat_array = arr.flat
    # some weird way to save coords
    curr_coords = flat_array.coords
    
    for item in flat_array:
        if type(item) == np.void:
            # if it's numpy.void, it's
            # complicated type
            add_numpy_void(xml_doc, arr_node, \
                           item, coords=curr_coords)
        elif type(item) == np.ndarray:
            # can't get here
            raise Exception, "Something went completely wrong"
        else:
            # it's just a plain type
            # add it's coords and value
            add_plain_item(xml_doc, arr_node, \
                           item, coords=curr_coords)
            
        # get next coords
        curr_coords = flat_array.coords
        # for loop end (hate python indents)
    
    parent_node.appendChild(arr_node)
    # serialize array function end

# creates dom item for plain type e.g. int32/float64/etc
def add_plain_item(xml_doc, parent_node, item, coords=None, name=None):
    item_node = xml_doc.createElement( get_class_name(item) )

    if not coords == None:
        item_node.setAttribute('coords', str(coords))

    if not name == None:
        item_node.setAttribute('name', name)

    item_value = xml_doc.createTextNode(str(item))
    item_node.appendChild(item_value)

    # add item to parent
    parent_node.appendChild(item_node)
    # end of add_plain_item function

# creates dom item for numpy.void type and
# serialize it's contents
def add_numpy_void(xml_doc, parent_node, item, coords=None, name=None):
    void_node = xml_doc.createElement( get_class_name(item) )
    # set needed attributes
    if not name == None:
        void_node.setAttribute('name', name)

    if not coords == None:
        void_node.setAttribute('coords', str(coords))
    
    void_node.setAttribute('dtype', str(item.dtype))

    # serialize void item itself
    serialize_numpy_void(xml_doc, void_node, item)
    parent_node.appendChild(void_node)
    # end of add_numpy_void

# serialize numpy.void item with list of inner items
def serialize_numpy_void(xml_doc, void_node, void_item, coords=None):
    dt = void_item.dtype
    
    if not dt.fields == None:
        for key, value in dt.fields.items():
            subitem = void_item[key]
            # check type of each item
            if type(subitem) == np.ndarray:
                # serialize array recursively
                serialize_array(xml_doc, void_node, \
                                void_item[key], key)
            elif type(subitem) == np.void:
                add_numpy_void(xml_doc, void_node, \
                               subitem, name=key)
            else:
                # else it's just plain type
                add_plain_item(xml_doc, void_node, \
                               subitem, name=key)
                #
    elif not dt.subdtype == None:
        # TODO maybe write something in this case
        pass
                
    # end of serialize_numpy_void function
