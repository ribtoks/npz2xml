import numpy as np
import types
import string

def dtype_from_string(line):
    # maybe change later
    # TODO improve security

    # first try to create some plain type
    dt = plain_dtype_from_string(line)
    if not dt is None:
        return dt
    
    try:
        dt = np.dtype(eval(line, dict(__builtins__=None)))
        return dt
    except SyntaxError:
        return "Cannot parse dtype object <[%s]>" % line
    except:
        # TODO do something in this case
        return "Error happened while converting <[%s]>" % line

def plain_dtype_from_string(line):
    try:
        dt = np.dtype(line)
        # do some stuff with dt
        return dt
    except:
        return None
    
def my_int(val_str):
    x = val_str.strip()
    if x.isdigit():
        return int(x)
    return None

def is_correct_plain_type(element, plain_type):
    try:
        arr = np.empty(1, dtype=plain_type)
        arr[0] = element
        return True
    except:
        return False

# returns xor of all numbers
# from 0 to num-1
def my_xor(num):
    results = [0, num - 1, 1, num]
    return results[num % 4]

# some weird way to do it
# TODO change it in future
def product(iter):
    prod = 1
    for x in iter:
        prod *= x
    return prod

# converts multidimension index to integer
def index_to_int(index, shape):
    if not len(index) == len(shape):
        raise Exception
    if len(index) == 0:
        return 0

    s = 0
    prod = 1
    for i in reversed(xrange(len(shape))):
        s += prod * index[i]
        prod *= shape[i]
    return s

def int_to_index(num, shape):
    raise Exception, "Not implemented"

# need to handle next cases
# "(2, )" -> (2,)
# "(2,3   ,  4,)" -> (2,3,4)
def parse_tuple(tuple_string):
    return tuple([my_int(item.strip()) for item in tuple_string[1:-1].split(',')\
                      if item.strip().isdigit()])
def parse_str(x):
    return x.isalpha() and x or x.isdigit() and \
        int(x) or x.isalnum() and x or \
        len(set(string.punctuation).intersection(x)) == 1 and \
        x.count('.') == 1 and float(x) or x
