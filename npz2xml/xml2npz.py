import os
import types
import numpy_helper as nh
import numpy as np
import xml.etree.ElementTree as etree

# TODO maybe later also take some converter
# argument for e.g. hex numbers in string to int

def deserialize(xml_npz_filepath, needs_validation=True):
    if not os.path.exists(xml_npz_filepath):
        raise IOError, "Xml file not found"
    
    xml_tree = etree.parse(xml_npz_filepath)
    root = xml_tree.getroot()

    # TODO change it somehow later
    # (not all xml files will have array names)
    arrays_dict = {}
    names_count = 0

    for child in list(root):
        # construct each array before it's parsing
        # (maybe it's ideological mistake, but anyway...)
        array_type = nh.dtype_from_string( child.attrib['dtype'] )
        array_shape = shape = nh.parse_tuple( child.attrib['shape'] )
        arr = np.empty(array_shape, array_type)

        # get it's name
        name = "f%d" % names_count
        if child.attrib.has_key('name'):
            name = child.attrib['name']
        else:
            names_count += 1
        # and now parse it
        deserialize_array(child, arr)

        arrays_dict[name] = arr
            
    return arrays_dict

def deserialize_array(node, numpy_object):    
    array_type = nh.dtype_from_string( node.attrib['dtype'] )

    for child in list(node):
        coords = nh.parse_tuple(child.attrib['coords'])

        if child.tag.endswith("void"):
            deserialize_void(child, numpy_object[coords])
        else:
            # else it's plain type
            numpy_object[coords] = deserialize_plain_item(child, array_type)
            
def deserialize_void(node, numpy_object):
    void_type = nh.dtype_from_string( node.attrib['dtype'] )
    
    for child in list(node):
        child_name = child.attrib['name']
        item_type = void_type[child_name]

        if child.tag.endswith('ndarray'):
             deserialize_array(child, numpy_object[child_name])
        elif child.tag.endswith("void"):
            deserialize_void(child, numpy_object[child_name])
        else:
            numpy_object[child_name] = deserialize_plain_item(child, item_type)

def deserialize_plain_item(node, item_type):
    if len(node.text) > 0:
        return nh.parse_str(node.text)
    return ''
                     
            

